# Développer chez Pepperbay

## Conventions de code

- [Convention PHP](docs/convention.md)
- [Convention HTML](docs/html/readme.md)
- [Convention SASS/CSS](docs/sass-css/readme.md)

## Process de développement

- [Code review](docs/codeReview.md)
- [Les conventions git](docs/git/readme.md)
    + Branching model : utiliser [Git flow](docs/git/readme.md#examples)
    + [Convention de commit](docs/git/readme.md#commit-message)

## Proposer une modification

N'hésite pas à proposer une modification sur la guideline si un point ne te semble pas adapté.

Voici la procédure à suivre:
- clone le dépôt en local
- crée une nouvelle branche suffixé par *-proposal
- pousse ta branche avec ta modification
- crée une merge request sous gitlab
- partage le lien avec l'équipe afin de discuter du changement
- attendre les échanges et les votes de l'équipe

Afin de valider la modification il est nécessaire d'avoir la majorité absolue. (et oui c'est la démocratie à pepperbay !)
