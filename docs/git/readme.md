# Convention GIT

## <a name='TOC'></a>Table des matières

  1. [Les dépôts](#repositories)
  1. [Les branches](#branching)
  1. [Exemples concrets](#examples)
    1. [Récupération du projet](#example-1)
    1. [Commencer une nouvelle fonctionnalité (feature-*)](#example-2)
    1. [Corriger des bugs sur le branche develop (develop)](#example-3)
    1. [Livrer une nouvelle release (release-*)](#example-4)
    1. [Corriger des bugs sur le branche release (release-*)](#example-5)
    1. [Livrer sur l'envrionnement de production (master)](#example-6)
    1. [Corriger un bug BLOQUANT sur la branche master (hotfix)](#example-7)
  1. [Les messages de commit](#commit-message) (optionnel)
  1. [Trucs et astuces](#tips-tricks)
    1. [Gitg](#gitg)
    1. [Config](#config)
  1. [Références](#credits)

## <a name='repositories'></a>Les Dépôts
### Nommage

```
# Le nom du dépôt doit être en snake-case (séparateur tiret et en minuscule)
<project?>-<techno?>-<name?>-<type>
```

[Pour plus d'information sur snake-case](https://en.wikipedia.org/wiki/Snake_case) :snake:

- **Project**: Le nom du projet (trade-easy, dexter, marine, etc...)
- **Techno**: La techno peut-être un langague ou un framework (php, js, symfony, angular, etc...)
- **Name**: Choisir un nom avec l'équipe mais vous êtes libre de choisir le terme que vous voulez
- **Type**: Principe du dépôt
  - ***App***: Une application complète
  - ***Library***: Une librairie.
  - ***API***: Une API.
  - ***Client***: Un client pour une API.
  - ***Cli***: Une ligne de commande.
  - ***Deploy***: Quelque chose relatif au déploiement.
  - ***Convention***: Règle de convention ou usage pour une technologie.

### Exemples
```
# <project>-<type>
trade-easy-app
trade-easy-deploy

# <project>-<techno>-<type>
dexter-ng-client

# <techno>-<type>
git-convention

# <techno>-<name>-<type>
php-toolbox-library
```


## <a name='branching'></a>Les branches

Pour les branches, pepperbay utilise le workflow git de [Vincent Driessen](http://nvie.com/posts/a-successful-git-branching-model/)

![git workflow](./git-branching-model.png "Git workflow by Vincent Driessen")

Pour en simplifier l'application il est fortement recommandé d'utiliser l'outil [git-flow](http://jeffkreeftmeijer.com/2010/why-arent-you-using-git-flow/). ([github](https://github.com/nvie/gitflow))

```bash
# Installation de git flow
$ sudo apt-get install git-flow
```


Voici un détail des différentes branches:

  - **master**:

    > La branche la plus stable du logiciel. Elle est déployée en production.

  - **develop**:

    > La branche reflète les développements en cours dans l'itération.

  - **release/***:

    > La branche release découle de la branche develop et est mise à disposition de l'équipe recette.

  - **feature/***:

    > La branche de feature est créer pour chaque nouvelle fonctonnalité à ajouter dans le sprint. Elle découle de la branche de développement. (Nommage **ABC-XXX-Description** où XXX peut-être US ou ST)

  - **hotfix/***:

    > La branche de hotfix est créée pour corriger un bug bloquant sur l'environnement de production. Elle découle donc de la branche master.




## <a name='examples'></a>Exemples concrets

**Attention: vous devez avez git flow d'installé !**

#### <a name='example-1'></a>Récupération du projet
```bash
# Récupération du projet git
$ git clone git@gitlab.com:pepperbay/trade-easy-app.git
```

#### <a name='example-2'></a>Commencer une nouvelle fonctionnalité (feature-*)
```bash
# Mise à jour de toutes les branches
$ git pull

# Création d'une nouvelle fonctionnalité à partir de la branche develop
$ git flow feature start ABC-XXX-Description

# Développer sa fonctionnalité
$ git add ...
$ git commit ...

# Une fois votre fonctionnalité aboutie il est temps de la pousser sur gitlab pour la revue de code
# A ce moment là vous pouvez créer votre merge request (source => ma fonctionnalité / cible => develop)
$ git flow feature publish

# https://gitlab.com/pepperbay/trade-easy-app/merge_requests/new
> NE PAS OUBLIER: Créer un merge request (ABC-XXX-Description) => (develop) avec le label "feature"

# NE PAS OUBLIER de faire relire son US par le PO ainsi que par un développeur.
# Une fois validée par les deux partis vous pouvez merger votre fonctionnalité dans la branche develop

# Se synchroniser avec la branch develop pour éviter les conflits lors du merge
$ git pull --rebase origin develop

# Dans certain cas il est nécessaire de faire un --force pour pousser sa branche après un rebase
$ git push (--force) origin ABC-XXX-Description

# Une fois la fonctionalité finalisée et nettoyée vous pouvez merger explicitement avec la branche develop
# Vous pouvez le faire avec l'interface graphique de gitlab ou en ligne de commande
# Si vous avez bien effectué les étapes plus haut vous n'aurez à ce niveau pas à gérer de conflit
$ git flow finish  --keepremote -F ABC-XXX-Description

# A ce moment vous devez vous retrouver sur la branche develop
# Pousser sur le serveur gitlab
$ git push origin develop

# Vous pouvez maintenant supprimer votre branche de feature
# En local (il se peut que git flow l'ai déjà supprimé pour vous)
$ git branch -d ABC-XXX-Description 
# Sur le serveur gitlab (vous pouvez aussi le faire via l'interface graphique https://gitlab.com/pepperbay/trade-easy-app/branches)
$ git push origin --delete ABC-XXX-Description
```

#### <a name='example-3'></a>Corriger des bugs sur le branche develop (develop)

```bash
# Mise à jour de toutes les branches
$ git pull

# Se mettre sur la branche develop à corriger
$ git checkout develop

# Si vous constatez que le bug nécessite plus de 1 commit, préviligiez la création d'une branche tel expliqué pour les fonctionnalités (FIX-123-branche)

# Corriger le bug directement sur le branche et faire un message de commit explicite
$ git commit -am "FIX(123): correction d'une faute d'orthographe"

# Pousser le correctif sur la branche develop
$ git push origin develop

```

#### <a name='example-4'></a>Livrer une nouvelle release (release-*)

```bash
# Mise à jour de toutes les branches
$ git pull

# Créer la nouvelle branche de release
$ git flow release start 18.0
# '18.0' correspond à la préparation de la livraison du sprint 18 (release)

# Pousser cette nouvelle branche sur le serveur
# A ce moment l'environnement de recette est déployé
$ git flow release publish

# https://gitlab.com/pepperbay/trade-easy-app/merge_requests/new
> NE PAS OUBLIER: Créer un merge request (Source : release/18.0) => (Target : master) avec le label "release"

# Ensuite, il faut supprimer toutes les branches mergées
#=> https://gitlab.com/pepperbay/trade-easy-app/branches 
# Bouton "Delete merged branches"

# et enfin vous pouvez corriger tous les bugs remontés.

```


#### <a name='example-5'></a>Corriger des bugs sur le branche release (release-*)

```bash
# Mise à jour de toutes les branches
$ git pull

# Se mettre sur le branche de release à corriger
$ git checkout release/18.0
# '18.0' correspond à la préparation de la livraison du sprint 18 (release)

#Pour corriger un bug sur la branche release il faut créer une sous branche de FIX

# Corriger le bug sur cette branche avec un message de commit explicite
# ATTENTION: sur la branche de release seules des corrections de bug sont attendues. Toute les nouvelles fonctionnalités sont à faire sur la branche develop
$ git commit -am "FIX(123): correction d'une faute d'orthographe"

$ git push origin release-fix-XXX

# Créer un merge request pour cette branche de fix pointant vers la release
# Demander une REVUE DE CODE par un pair
# Une fois le fix validé vous pouver merger la branche via l'interface
# ATTENTION: il faut bien que la branche pointe vers la branche RELEASE/XX.0


```

#### <a name='example-6'></a>Livrer sur l'envrionnement de production (master)

```bash
# Mise à jour de toutes les branches
$ git checkout master
$ git pull
$ git checkout develop
$ git pull

# Se mettre sur le branche de release
$ git checkout release/18.0
# '18.0' correspond à la préparation de la livraison du sprint 18 (release)

# Publier la branche release :
# La publication de la branche release se fait en 3 étapes
# 1) La branche release est mergé avec la branche master (si conflits les résoudres)
# 2) Un tag est créé pour cette nouvelle version (numéro de la release)
# 3) La nouvelle version (tag) est mergé avec la branche develop (si conflits les résoudres)

$ git flow release finish  --keepremote -F 18.0

# Résoudre éventuellement les conflits depuis la branche develop
# dans le commentaire du commit renseigner alors le numéro de release

# Pousser les branches et le nouveau tag sur le serveur
$ git push origin master
$ git push origin develop
$ git push --tags

# Vous pouvez maintenant supprimer votre branche de release
# En local si cela n'a pas déjà été fait automatiquement
$ git branch -d release/18.0
# Sur le serveur gitlab (vous pouvez aussi le faire via l'interface graphique https://gitlab.com/pepperbay/trade-easy-app/branches)
$ git push origin --delete release/18.0
# une erreur peut survenir si vous n'avez pas les droits suffisants.

```

Une fois avoir poussé les branches master et develop elles sont respectivement déployé sur l'environnement staging (preprod) et de review (démo).


**Attention :** Veuillez attendre que les pipelines soient terminés et qu'il y a aucun annomalie avant de de déployer en production. Vous serez notifié sur slack si tout se passe bien.
De plus il est préférable de vérifier l'environnement de preprod (se connecter et faire un petit tour du propriétaire ;) avant de déployer en production.


Les branches sont à jour, il ne reste plus qu'à passer à l'étape du [déploiement](https://gitlab.com/pepperbay/trade-easy-app/blob/develop/docs/deployement.md).

#### <a name='example-7'></a>Corriger un bug BLOQUANT sur la branche master (hotfix)

```bash
$ git checkout master

# Mise à jour de toutes les branches
$ git pull

# Créer une branche de HOTFIX pour corriger le bug bloquant sur la branche master
$ git flow hotfix start HOTFIXXXX_abchhkj

# A ce moment là créer une merge request pointant vers la branche MASTER
# Demander à faire une REVUE DE CODE par un pair
# Une fois le correctif validé par le pair vous pouvez publier le hotfix (ATTENTION PAS PAR l'INTERFACE !!!)

# Publier le correctif
$ git flow hotfix publish HOTFIXXXX_abchhkj

# https://gitlab.com/pepperbay/trade-easy-app/merge_requests/new
> NE PAS OUBLIER: Créer un merge request (Source : hotfix/HOTFIXXXX_abchhkj) => (Target : master) avec le label "hotfix"

# Fair le merge:
$ git flow hotfix finish --keepremote -F HOTFIXXXX_abchhkj
# !!! -> renseigner nouveau tag avec nano exemple 2.1 (partie nano où il y a : # Écrire un message pour l'étiquette )

# ATTENTION si vous avez des conflits, les résoudre
#  une fois résolus il suffit de lancer la commande "git commit" le message est pré-rempli
# ATTENTION à la fin du git flow hotfix finish on est positionné sur la branche DEVELOP

# Pousser les branches et le nouveau tag sur le serveur
# Livraison du HOTFIX sur develop
$ git push origin develop
# Livraison du HOTFIX sur master
$ git checkout master
$ git push origin master

$ git push --tags 

# !!! suprimmer la branche de hotfix sur le serveur (hotfix/HOTFIXXXX_abchhkj) par gitlab
```


## <a name='commit-message'></a>Commit message conventions (optionnal)

Commit message must be readable, understandable and write in english.

### TL;DR

![Convention de commit](../assets/commit.png)

### Format

Les messages de commits sont rédigés en français. (sauf le scope pour que ça soit plus court)

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

Le header (la première ligne, composée de `<type>`, `<scope>`, et `<subject>`) est obligatoire.
Dans le header seul le scope est facultatif.


* `<type>`, valeurs possibles :
    * **feat** : nouvelle fonctionnalité
    * **fix** : correction de fonctionnalité
    * **refactor** : changements de code qui n'apportent ni corrections ni nouvelles fonctionnalités
    * **style** : changements cosmétiques du code (CS, white-spaces, formattage, etc.)
    * **test** : ajout, modification ou correction de tests
    * **chore** : changements qui ne concerne pas directement le code (ex : configuration de déploiement, outils auxiliaires)
    * **docs** : ajout, modification ou correction de la documentation
* `<scope>` : définit la partie du code modifiée, (ex: search, user profile, etc.)
* `<subject>` : description courte des changements apportés par le commit
* `<body>` : description détaillée, peut inclure les raison de choix effectué
* `<footer>` : références, par exemple vers les issues concernés

Pour plus de "fun" utilisez les [emojis](http://www.emoji-cheat-sheet.com/) :+1:.


## <a name='tips-tricks'></a>Trucs et astuces

### <a name='gitg'></a>Utiliser Gitg :bulb:

[Gitg](https://wiki.gnome.org/Apps/Gitg) est un outils graphique pour git. Il est léger et rapide. Il est l'outil idéal pour visualiser l'historique git en mode hors ligne. Il permet aussi d'effectuer vos commits de manière atomique.

### <a name='config'></a>Configuration globale

Mettre le filemode à false pour ignorer le changement de mode des fichiers.

```bash    
    git config --global core.filemode false
```

Si vous disposez de git < 2.0 alors il est recommandé de forcer le plus simple pour le push. Il permet de ne pusher ou pull que la branche locale ([en savoir plus]( http://stackoverflow.com/questions/13148066/warning-push-default-is-unset-its-implicit-value-is-changing-in-git-2-0)
```bash  
    git config --global push.default simple
```

Vous pouvez inciter git à utiliser le rebase au lieu du merge par défaut.
```bash    
git config --global branch.autosetuprebase always
git config --global pull.rebase preserve #(this is a very recent and useful addition that appeared in git 1.8.5)
```

Supprimer tout les branches locales qui ne sont plus présentes sur le serveur distant.
```bash    
git config --global fetch.prune true
```

Pour améliorer votre productivité vous pouvez ajouter des alias:

```bash
git config --global alias.co checkout
git config --global alias.ci commit
git config --global alias.st status
git config --global alias.br branch

# Get all history branch more graphically like network
git config --global alias.net "log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"

git config --global alias.unstage 'reset HEAD --'
git config --global alias.last 'log -1 HEAD'

#Required gitg
git config --global alias.ui "!gitg"
```

Comment voir toutes mes modifications en cours ?
```bash    
git diff
git diff (path/to/file)
```

Annuler les modifications pour un fichier ?
```bash    
git checkout -- (path/to/file)
```

Annuler toutes les modification en cours ?
```bash    
git reset --hard
```

J'ai basculé de branche et j'ai des fichiers qui sont non versionné comment les enlever ?
```bash    
git clean -d -x -f
```


Comment annuler mon dernier commit tout en restaurant mes fichiers ?
```bash    
git reset HEAD~1
```

Comment vraiment annuler mon dernier commit ? (Attention une fois pousser vous ne pourrez plus récupérer le dernier commit)
```bash    
git reset --hard HEAD~1
```

Comment annuler un rebase ou un merge avec des conflits ? (cela revient au statut du dépôt avant le rebase ou le merge)
```bash    
# pour le rebase
git rebase --abort

# pour le merge
git merge --abort
```

Autres commandes pratiques:

- git [stash](https://git-scm.com/docs/git-stash)
- git [bisect](https://git-scm.com/docs/git-bisect)

## <a name='credits'></a>Références

* Git++ : Passez au niveau supérieur de la gestion de version
    * [la conférence](https://www.youtube.com/watch?v=m0_C2cfM9IM)
* [Convention de commit angular](https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md#-git-commit-guidelines)
* Le workflow git flow [#1](https://about.gitlab.com/2016/07/27/the-11-rules-of-gitlab-flow/), [#2](https://docs.gitlab.com/ee/workflow/gitlab_flow.html), [#3](http://blogs.atlassian.com/2014/01/simple-git-workflow-simple/)
